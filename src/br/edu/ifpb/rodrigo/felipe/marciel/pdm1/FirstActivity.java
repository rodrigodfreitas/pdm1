package br.edu.ifpb.rodrigo.felipe.marciel.pdm1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FirstActivity extends Activity {

	Button bt1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);

		bt1 = (Button) findViewById(R.id.bt1);

		bt1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent in1 = new Intent(FirstActivity.this,
						SecondActivity.class);
				Toast.makeText(FirstActivity.this,
						"Tarefas!",
						Toast.LENGTH_SHORT).show();
				startActivity(in1);

			}
		});
	}
}
