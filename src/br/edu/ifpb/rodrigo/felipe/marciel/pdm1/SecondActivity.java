package br.edu.ifpb.rodrigo.felipe.marciel.pdm1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SecondActivity extends Activity {

	Button bt2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
	
		bt2 = (Button) findViewById(R.id.bt2);
		
		bt2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent in2 = new Intent(SecondActivity.this,
						FirstActivity.class);
				Toast.makeText(SecondActivity.this,
						"Dashboard!",
						Toast.LENGTH_SHORT).show();
				startActivity(in2);

			}
		});
	}

}
